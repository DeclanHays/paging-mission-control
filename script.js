const fs = require('fs');
/*
Ingest status telemetry data and create alert messages for the 
  following violation conditions:

If for the same satellite there are three battery voltage 
  readings that are under the red low limit within a five minute 
  interval.
*/

var outputJSONArray = [];
var entireFile = "";

// key: sat ID, value: boolean
//  value == true means the error has been found for that satellite, false otherwise
var [thermostatWarning, batteryWarning] = [new Map(), new Map()];
var [thermostatMap, batteryMap] = [new Map(), new Map()];
// key: sat ID, value: array of times

// Reading command to run program
process.stdin.setEncoding("utf8");

if (process.argv.length != 3) {
  process.stdout.write('Usage: node script.js fileName');
  process.exit(1);
}
// The file we will be reading from is the third argument
fileName = process.argv[2];

// Fails of the specific file does not exist
try {
  entireFile = fs.readFileSync(fileName, 'utf8');
} catch (err) {
  console.error(err);
}

// Creates an array containing each line of the file
let lines = entireFile.split('\n')

/*
Loop through each line, checking for issues in satellites.
*/
for (let i = 0; i < lines.length; i++) {
  
  // Getting important values from each line 
  let line = lines[i].split("|");
  let [time, satID, redHighLim, 
        yellowHighLim, yellowLowLim, 
        redLowLim, rawVal, component] = [line[0], Number(line[1]), Number(line[2]), 
                                        line[3], line[4], 
                                        Number(line[5]), Number(line[6]), line[7]];

  /* In each line we will 
    1. Identify it as a reading for a battery or a thermostat
    2. Check if it has any readings that are noteworthy
      2a. TSTAT: rawVal > red high limit
          BATT: rawVal < red low limit
    3. If so, then we 
      3a. Add the new reading to that satellite's value in the proper map
      3b. Purge irrelevant readings (5+ minutes old)
      3c. Check if this puts us over the limit (3 in 5 mins)
  */
  if (component == "TSTAT") {
    // If a thermostat's rawVal > redHighLim, add the time to that satID's 
    //  value in thermostatMap
    if (Number(rawVal) > Number(redHighLim)) {

      // If that satellite already has problematic reading(s), add the new 
      //  reading to satID's value in thermostatMap
      if (thermostatMap.get(satID)) { 
        // readings is used to add the new problematic reading to the map
        readings = thermostatMap.get(satID);
        readings.push(time);

        // Add new reading 
        thermostatMap.set(satID, readings);

        // Now we purge the times for that satellite. 
        thermostatMap.set(satID, purgeTimes(thermostatMap.get(satID), time));

        // If this satellite still has 3+ problematic readings 
        if (thermostatMap.get(satID).length >= 3 && !thermostatWarning.get(satID)) {
          
          // thermostatWarning is used to limit recorded issues to 1 
          //  per satellite per componenet
          thermostatWarning.set(satID, true) 
          let time = formatTime(thermostatMap.get(satID)[0]);

          outputJSONArray.push( {
            satelliteId: satID,
            severity: "RED HIGH",
            component: "TSTAT",
            timestamp: time
          } );

        }

      } else {
        // If this satID is not in the map yet, add it and set value to an 
        //  array that contains the current time.
        thermostatMap.set(satID, [time]);
      }

    }

  } else if (component == "BATT") {

    if (Number(rawVal) < Number(redLowLim))

      // Check if this satellite is in the map already.
      if (batteryMap.get(satID)) { 

        // readings is used to add the new problematic reading to the map
        readings = batteryMap.get(satID);
        readings.push(time);

        // Add new reading
        batteryMap.set(satID, readings);

        // Now we purge the times for that satellite. 
        batteryMap.set(satID, purgeTimes(batteryMap.get(satID), time));

        // If this satellite still has 3+ problematic readings 
        if (batteryMap.get(satID).length >= 3 && !batteryWarning.get(satID)) {
          // batteryWarning is used to limit recorded issues to 1 
          //  per satellite per componenet
          batteryWarning.set(satID, true);

          let time = formatTime(batteryMap.get(satID)[0]);
          
          outputJSONArray.push ( {
            satelliteId: satID,
            severity: "RED LOW",
            component: "BATT",
            timestamp: time
        } );

      }

    } else {
      // If this satID is not in the map yet, add it and set value 
      //  to an array that contains the current time
      batteryMap.set(satID, [time]);
    }

  }

}
console.log(outputJSONArray);



/* 
purgeTimes takes an array of times and the current time and returns an array
  of times that are within 5 minutes of the current time.
*/
function purgeTimes(timeArray, currTime) {
  let currDate = currTime.split(':')[0].split(' ')[0];
  let currTimeHour = currTime.split(':')[0].split(' ')[1];
  let currTimeMinutes = currTime.split(':')[1];

  // Check each time in timeArray
  for (let i = 0; i < timeArray.length; i++) {
    let date = timeArray[i].split(':')[0].split(' ')[0];
    let hour = timeArray[i].split(':')[0].split(' ')[1];
    let minutes = timeArray[i].split(':')[1];

    // If the dates (years, months, or days), hours, or minutes are different, remove it from the array.
    if (currDate != date || hour != currTimeHour || Number(currTimeMinutes) - Number(minutes) >= 5) { 
      //more than 5 minutes have passed, remove it from the array
      timeArray.splice(i, 1); //index to remove, number to remove
      i--;
    }
    
  }
  return timeArray;
}

/*
formatTime takes the current time as a string and converts it to ISO 8601
*/
function formatTime(dateTime) {
  year = dateTime.slice(0,4);
  month = dateTime.slice(4,6);
  day = dateTime.slice(6,8);
  time = dateTime.slice(9);
  formattedTime = `${year}-${month}-${day}T${time}Z`;

  return formattedTime;
}

